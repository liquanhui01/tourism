package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"

	co "shopping/configs"
	ro "shopping/routers"
)

func main() {
	// Get configration information
	co.InitConfig()
	gin.SetMode(viper.GetString("mode"))

	route := ro.InitRouter()
	srv := &http.Server{
		Addr:    viper.GetString("server.host"),
		Handler: route,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("failed to run serve")
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server shutdown:%s\n", err)
	}
	log.Fatalf("Server exiting")
}
