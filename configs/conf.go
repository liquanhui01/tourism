package conf

import (
	"fmt"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"

	"shopping/pkg"
)

var (
	cfg  = pflag.StringP("conf", "c", "", "configuration file.")
	help = pflag.BoolP("help", "h", false, "Show this help message.")
)

func InitConfig() {
	// var once sync.Once
	var once pkg.Once
	err := once.Do(func() error {
		pflag.Parse()
		if *help {
			pflag.Usage()
			return nil
		}

		// Read configuration from file
		if *cfg != "" {
			viper.SetConfigFile(*cfg)   // Specify the name of the configuration file
			viper.SetConfigType("yaml") // Specify the extension in the configuration file
		} else {
			viper.AddConfigPath(".") // Add the current directory to the search path of configuration file
			viper.AddConfigPath("$HOME/workplace/shopping/configs")
			viper.SetConfigName("conf") // Configuration file name
		}

		if err := viper.ReadInConfig(); err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}
}
