package routers

import (
	"github.com/gin-gonic/gin"

	user "shopping/internal/apiserver/controller/v1/user"
	"shopping/internal/apiserver/store/mysql"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	// r.Use(auth.AuthFunc())
	storeIns, _ := mysql.GetMySQLFactoryOr(nil)
	userv1 := r.Group("/api/v1")
	{
		userController := user.NewUserController(storeIns)

		userv1.POST("", userController.Create)
	}

	return r
}
