/*
Navicat MySQL Data Transfer
Source Database       : shopping_mall
Target Server Type    : MYSQL
Target Server Version : 50639
File Encoding         : 65001
Date: 2021-10-21
*/

USE shopping;

-- -----------------------------------
-- Table structure for users
-- -----------------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
    `id` int(100) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `open_id` varchar(150) NOT NULL COMMENT '微信用户id',
    `user_name` varchar(100) NOT NULL COMMENT '微信用户昵称',
    `user_code` varchar(50) NOT NULL COMMENT '邀请码',
    `user_avatar` Text NULL COMMENT '用户头像',
    `is_commission` boolean NOT NULL DEFAULT FALSE COMMENT '是否为分销商',
    `created_at` datetime NOT NULL COMMENT '创建时间',
    `updated_at` datetime NOT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户管理';

-- ------------------------------------
-- Table structure for user_category
-- ------------------------------------
DROP TABLE IF EXISTS `shop_category`;
CREATE TABLE `shop_category` (
    `id` int(100) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `category_name` varchar(100) NOT NULL COMMENT '商户分类名',
    `created_at` datetime NOT NULL COMMENT '创建时间',
    `updated_at` datetime NOT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商户分类管理';

-- ------------------------------------
-- Table structure for shop
-- ------------------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
    `id` int(100) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `shop_name` Text NOT NULL COMMENT '商户名称',
    `password` Text NOT NULL COMMENT '密码',
    `shop_avatar` Text NULL COMMENT '商户头像',
    `shop_phone` varchar(20) NULL DEFAULT '' COMMENT '商户联系方式',
    `shop_work_time` Text NULL COMMENT '商户营业时间',
    `shop_address` Text NULL COMMENT '商户地址',
    `category_id` int(100) unsigned NOT NULL COMMENT '所属分类',
    `is_active` boolean DEFAULT FALSE COMMENT '账户是否激活',
    `is_cancellate` boolean DEFAULT FALSE COMMENT '账户是否注销',
    `created_at` datetime NOT NULL COMMENT '创建时间',
    `updated_at` datetime NOT NULL COMMENT '更新时间',
    FOREIGN KEY (category_id) REFERENCES shop_category(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商户管理';

