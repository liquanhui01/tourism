package server

import (
	"shopping/internal/pkg/middleware"
	"shopping/internal/pkg/middleware/auth"
)

// NewBasicAuth
func NewBasicAuth() middleware.AuthStrategy {
	return auth.NewBasicStrategy(func(userName, openid string) bool {
		// TODO
		return true
	})
}

// NewJWTAuth
func NewJWTAuth() middleware.AuthStrategy {
	return auth.NewJWT()
}
