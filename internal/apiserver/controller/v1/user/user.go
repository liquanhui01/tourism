package user

import (
	srvv1 "shopping/internal/apiserver/service/v1"
	"shopping/internal/apiserver/store"
)

type UserController struct {
	srv srvv1.Service
}

func NewUserController(store store.Factory) *UserController {
	return &UserController{
		srv: srvv1.NewService(store),
	}
}
