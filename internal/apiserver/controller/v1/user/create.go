package user

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	rp "shopping/internal/apiserver/store/repo"
)

func (u *UserController) Create(c *gin.Context) {
	fmt.Println("user create function called.")

	var r rp.User

	if err := c.ShouldBindJSON(&r); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err, "data": nil})
		return
	}

	// Insert the user to the storage
	id, err := u.srv.Users().Create(c, &r)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err, "data": nil})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"msg":  "ok",
		"data": id,
	})
}
