package v1

import (
	"context"
	"fmt"

	"shopping/internal/apiserver/store"
	rp "shopping/internal/apiserver/store/repo"
)

// UserSrv defines functions used to handle user request
type UserSrv interface {
	Create(ctx context.Context, user *rp.User) (uint, error)
	Get(ctx context.Context, id uint) (*rp.User, error)
	Update(ctx context.Context, user *rp.User) error
}

type userService struct {
	store store.Factory
}

var _ UserSrv = (*userService)(nil)

func newUsers(srv *service) *userService {
	return &userService{store: srv.store}
}

func (u *userService) Create(ctx context.Context, user *rp.User) (uint, error) {
	fmt.Println("进入到service层面")
	id, err := u.store.Users().Create(ctx, user)
	if err != nil {
		fmt.Println("发生错误")
		return 0, err
	}
	return id, nil
}

func (u *userService) Get(ctx context.Context, id uint) (*rp.User, error) {
	user, err := u.store.Users().Get(ctx, id)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (u *userService) Update(ctx context.Context, user *rp.User) error {
	return u.store.Users().Update(ctx, user)
}
