package repo

import "time"

type User struct {
	ID           uint      `gorm:"primaryKey;unique;autoIncrement" json:"id"`
	OpenId       string    `gorm:"not null;" json:"open_id"`
	UserName     string    `gorm:"not null;" json:"user_name"`
	UserCode     string    `gorm:"not null;" json:"user_code"`
	UserAvatar   string    `gorm:"default:''" json:"user_avatar"`
	IsCommission bool      `gorm:"not null; default:false" json:"is_commission"`
	CreatedAt    time.Time `gorm:"not null" json:"created_at"`
	UpdatedAt    time.Time `gorm:"not null" json:"updated_at"`
}

// TableName maps to mysql table name
func (u *User) TableName() string {
	return "user"
}
