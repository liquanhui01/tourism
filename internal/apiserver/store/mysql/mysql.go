package mysql

import (
	"fmt"
	"sync"

	"github.com/pkg/errors"
	"gorm.io/gorm"

	"shopping/internal/apiserver/store"
	"shopping/internal/pkg/options"
	"shopping/pkg/db"
)

type datastore struct {
	db *gorm.DB
	// can include two database instance if needed
	// docker *gorm.DB
	// db *gorm.DB
}

func (ds *datastore) Users() store.UserStore {
	return newUsers(ds)
}

func (ds *datastore) Close() error {
	db, err := ds.db.DB()
	if err != nil {
		return errors.Wrap(err, "Failed to get gorm db instance.")
	}
	return db.Close()
}

var (
	mysqlFactory store.Factory
	once         sync.Once
)

func GetMySQLFactoryOr(opts *options.MySQLOptions) (store.Factory, error) {
	if opts == nil && mysqlFactory == nil {
		return nil, fmt.Errorf("Failed tp get mysql factory")
	}

	var err error
	var dbIns *gorm.DB

	once.Do(func() {
		// options := &db.Options{
		// 	Host:                  opts.Host,
		// 	Username:              opts.Username,
		// 	Password:              opts.Password,
		// 	Database:              opts.Database,
		// 	MaxIdleConnections:    opts.MaxIdleConnections,
		// 	MaxOpenConnections:    opts.MaxOpenConnections,
		// 	MaxConnectionLifeTime: opts.MaxConnectionLifeTime,
		// 	LogLevel:              opts.LogLevel,
		// }

		options := &db.Options{
			Host:                  "127.0.0.1",
			Username:              "root",
			Password:              "liqh930215",
			Database:              "shopping",
			MaxIdleConnections:    100,
			MaxOpenConnections:    100,
			MaxConnectionLifeTime: 10,
			LogLevel:              2,
		}

		dbIns, err = db.New(options)

		mysqlFactory = &datastore{dbIns}
	})

	if mysqlFactory == nil || err != nil {
		return nil, fmt.Errorf("Failed to get mysql store factory, mysqlFactory: %+v, error: %w", mysqlFactory, err)
	}

	return mysqlFactory, nil
}
