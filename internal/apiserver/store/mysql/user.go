package mysql

import (
	"context"
	"fmt"

	"gorm.io/gorm"

	rp "shopping/internal/apiserver/store/repo"
)

type users struct {
	db *gorm.DB
}

func newUsers(ds *datastore) *users {
	return &users{db: ds.db}
}

// Create creates a new user account
func (u *users) Create(ctx context.Context, user *rp.User) (uint, error) {
	fmt.Println("进入到mysql层")
	return user.ID, u.db.WithContext(ctx).Create(user).Error
}

// FindUser search user account by id
func (u *users) Get(ctx context.Context, id uint) (user *rp.User, err error) {
	return user, u.db.WithContext(ctx).First(&user, id).Error
}

// Update function update user account
func (u *users) Update(ctx context.Context, user *rp.User) error {
	return u.db.WithContext(ctx).Model(&user).Updates(user).Error
}
