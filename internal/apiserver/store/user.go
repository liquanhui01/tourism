package store

import (
	"context"

	rp "shopping/internal/apiserver/store/repo"
)

// UserStore defines the user storage interface
type UserStore interface {
	Create(ctx context.Context, user *rp.User) (uint, error)
	Get(ctx context.Context, id uint) (*rp.User, error)
	Update(ctx context.Context, user *rp.User) error
}
