package store

var client Factory

// Factory defines the shopping plateform storage interface.
type Factory interface {
	Users() UserStore
}

// Client return the store client instance.
func Client() Factory {
	return client
}

// SetClient set the shopping store client.
func SetClient(factory Factory) {
	client = factory
}
