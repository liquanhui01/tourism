package auth

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"

	"shopping/internal/pkg/middleware"
)

type JWTStrategy struct {
	SigningKey []byte
}

type Claims struct {
	Flag string
	jwt.StandardClaims
}

var _ middleware.AuthStrategy = &JWTStrategy{}

// NewJWT initlize JWT
func NewJWT() *JWTStrategy {
	return &JWTStrategy{
		[]byte(viper.GetString("jwt.key")),
	}
}

// GenerateJWT defines a method to generate JWT
func (j *JWTStrategy) GenerateJWT(userId int64) (string, error) {
	expiredTime := viper.GetInt("jwt.timeout")
	ex := time.Now().Add(time.Duration(expiredTime) * time.Hour)
	claims := &Claims{
		Flag: j.GenerateUserFlag(userId), // Generate a unique user identification
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: ex.Unix(),                 // Timeout
			IssuedAt:  time.Now().Unix(),         // Awarding time
			Issuer:    viper.GetString("issuer"), // Issuer
			NotBefore: time.Now().Unix(),         // Effective time
			Subject:   "user token",
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(j.SigningKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// ParseToken defines a method to parse token
func (j *JWTStrategy) ParseToken(tokenString string) (*Claims, error) {
	tokenClaims, err := jwt.ParseWithClaims(tokenString, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return j.SigningKey, nil
	})
	if tokenClaims != nil {
		if claims, ok := tokenClaims.Claims.(*Claims); ok {
			return claims, nil
		}
	}
	return nil, err
}

// AuthFunc middleware
func (j *JWTStrategy) AuthFunc() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Parsing token authentication through http header
		token := c.GetHeader("Authorization")
		fmt.Println("token is ", token)
		if token == "" {
			c.JSON(http.StatusForbidden, gin.H{
				"status": -1,
				"msg":    "请求未携带token，无访问权限",
				"data":   nil,
			})
			c.Abort()
			return
		}

		// initlize a jwt object instance and parse the token according to the structure method.
		j := NewJWT()
		// parse the relevant information contained in the token payload.
		realToken := strings.Fields(token)[1]
		claims, err := j.ParseToken(realToken)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{
				"status": -1,
				"msg":    err.Error(),
				"data":   nil,
			})
			c.Abort()
			return
		}
		// Write the claims in token payload to gin.Context object.
		c.Set("claims", claims)
	}
}

// GenerateUserFlag defines a method to generate the only user's flag.
func (j *JWTStrategy) GenerateUserFlag(userId int64) string {
	md5 := md5.New()
	md5.Write([]byte(strconv.FormatInt(userId, 10)))
	return hex.EncodeToString(md5.Sum([]byte("")))
}
