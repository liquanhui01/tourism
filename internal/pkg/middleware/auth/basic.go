package auth

import (
	"encoding/base64"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	"shopping/internal/pkg/middleware"
)

type BasicStrategy struct {
	compare func(userName, openId string) bool
}

var _ middleware.AuthStrategy = &BasicStrategy{}

func NewBasicStrategy(compare func(userName, openId string) bool) BasicStrategy {
	return BasicStrategy{compare: compare}
}

func (b BasicStrategy) AuthFunc() gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := strings.SplitN(c.Request.Header.Get("Authorization"), " ", 2)

		if len(auth) != 2 || auth[0] != "Basic" {
			c.JSON(http.StatusUnauthorized, gin.H{
				"status": -1,
				"msg":    "登陆失败",
				"data":   nil,
			})
			c.Abort()

			return
		}

		payload, _ := base64.StdEncoding.DecodeString(auth[1])
		pair := strings.SplitN(string(payload), ":", 2)

		if len(pair) != 2 || !b.compare(pair[0], pair[1]) {
			c.JSON(http.StatusUnauthorized, gin.H{
				"status": -1,
				"msg":    "登陆失败",
				"data":   nil,
			})
			c.Abort()

			return
		}
		c.Set("OpenIdKey", pair[1])
		c.Next()
	}
}
