package middleware

import (
	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
)

const (
	XRequestIDKey = "X-Request-ID"
)

func RequestID() gin.HandlerFunc {
	return func(c *gin.Context) {
		rid := c.GetHeader("X-Request-ID")

		if rid == "" {
			rid = uuid.NewV4().String()
		}

		c.Request.Header.Set(XRequestIDKey, rid)
		c.Set(XRequestIDKey, rid)

		c.Next()
	}
}
