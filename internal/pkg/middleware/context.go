package middleware

import (
	"github.com/gin-gonic/gin"
)

const OpenIdKey = "openid"

func Context() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("KeyRequestID", c.GetString(XRequestIDKey))
		c.Set("KeyOpenId", c.GetString(OpenIdKey))
	}
}
