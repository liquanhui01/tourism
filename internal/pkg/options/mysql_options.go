package options

import (
	"time"

	"github.com/spf13/pflag"
	"gorm.io/gorm"

	"shopping/pkg/db"
)

// MySQLOptions defines for mysql database.
type MySQLOptions struct {
	Host                  string        `json:"host, omitempty"                           mapstructure:"host"`
	Username              string        `json:"username, omitempty"                       mapstructure:"username"`
	Password              string        `json:"password, _"                               mapstructure:"password"`
	Database              string        `json:"database"                                  mapstructure:"database"`
	MaxIdleConnections    int           `json:"max-idle-connections, omitempty"           mapstructure:"max-idle-connections"`
	MaxOpenConnections    int           `json:"max-open-connections, omitempty"           mapstructure:"max-open-connections"`
	MaxConnectionLifeTime time.Duration `json:"max-connection-left-time, omitempty"       mapstructure:"max-connection-life-time"`
	LogLevel              int           `json:"log-level"                                 mapstructure:"log-level"`
}

// NewSQLOptions create a `zero` value instance.
func NewSQLOptions() *MySQLOptions {
	return &MySQLOptions{
		Host:                  "127.0.0.1:3306",
		Username:              "",
		Password:              "",
		Database:              "",
		MaxIdleConnections:    100,
		MaxOpenConnections:    100,
		MaxConnectionLifeTime: time.Duration(10) * time.Second,
		LogLevel:              1, // Silent
	}
}

// AddFlags adds flags to mysql storage for a specific APIServer to the specified FlagSet.
func (o *MySQLOptions) AddFlags(fs *pflag.FlagSet) {
	fs.StringVar(&o.Host, "mysql.host", o.Host, ""+
		"MySQL service host address. If left blank, the following related mysql options will be ignored.")
	fs.StringVar(&o.Username, "mysql.username", o.Username, ""+
		"Username for access to mysql service.")
	fs.StringVar(&o.Password, "mysql.password", o.Password, ""+
		"Password for access to mysql, should be used pair with password.")
	fs.StringVar(&o.Database, "mysql.database", o.Database, ""+
		"Database name for the server to use.")
	fs.IntVar(&o.MaxIdleConnections, "mysql.Max-idle-connections", o.MaxIdleConnections, ""+
		"Maxumum idle connections allowed to connect to mysql.")
	fs.IntVar(&o.MaxOpenConnections, "mysql.max-open-connections", o.MaxOpenConnections, ""+
		"Maximum open connections allowed to connect to mysql.")
	fs.DurationVar(&o.MaxConnectionLifeTime, "mysql.max-connection-lift-time", o.MaxConnectionLifeTime, ""+
		"Maximum connection lift time allowed to connect to mysql.")
	fs.IntVar(&o.LogLevel, "mysql.log-level", o.LogLevel, ""+
		"Specify gorm log level.")
}

func (o *MySQLOptions) NewClient() (*gorm.DB, error) {
	opts := &db.Options{
		Host:                  o.Host,
		Username:              o.Username,
		Password:              o.Password,
		Database:              o.Database,
		MaxIdleConnections:    o.MaxIdleConnections,
		MaxOpenConnections:    o.MaxOpenConnections,
		MaxConnectionLifeTime: o.MaxConnectionLifeTime,
		LogLevel:              o.LogLevel,
	}

	return db.New(opts)
}
