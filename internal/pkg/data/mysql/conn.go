package mysql

import (
	"database/sql"
	"log"
	"time"

	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func init() {
	sqlDB, err := sql.Open(
		viper.GetString("database.mysql.driver"),
		viper.GetString("database.mysql.dsn"),
	)
	if err != nil {
		log.Fatalf("mysql connection error, err is: %s\n", err.Error())
	}

	// connection pool settings
	sqlDB.SetMaxIdleConns(viper.GetInt("database.mysql.maxIdleConns"))
	sqlDB.SetMaxOpenConns(viper.GetInt("database.mysql.maxOpenConns"))
	sqlDB.SetConnMaxLifetime(time.Duration(viper.GetInt("database.mysql.maxLifttime")) *
		time.Hour)

	gormDB, err := gorm.Open(mysql.New(mysql.Config{
		Conn: sqlDB,
	}), &gorm.Config{})

	if err != nil {
		log.Fatalf("mysql connection error, err is: %s\n", err.Error())
	}

	DB = gormDB
}
